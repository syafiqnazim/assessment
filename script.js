const csv = require("csv-parser");
const fs = require("fs");

const results = [];

function compareValues(key, order = "asc") {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const itemA = typeof a[key] === "string" ? Number(a[key]) : a[key];
    const itemB = typeof b[key] === "string" ? Number(b[key]) : b[key];

    let comparison = 0;
    if (itemA > itemB) {
      comparison = 1;
    } else if (itemA < itemB) {
      comparison = -1;
    }
    return order === "desc" ? comparison * -1 : comparison;
  };
}

fs.createReadStream("data.csv")
  .pipe(csv({}))
  .on("data", (data) => results.push(data))
  .on("end", () => {
    // get model name that have shortest machine cycle time
    const shortestTime = results.sort(compareValues("MYCT: machine cycle time in nanoseconds"));
    console.log(`Model name that have shortest machine cycle time is >>`, shortestTime[0]["Model Name"]);

    // list top 10 model name that have highest cache memory
    const topTenCacheMemory = results.sort(compareValues("CACH: cache memory in kilobytes", "desc"));
    console.log(
      `Top 10 model name that have highest cache memory >>`,
      topTenCacheMemory.slice(0, 10).map((data, i) => ({ [i + 1]: data["Model Name"] }))
    );

    // list the next top 10 model name that have huge difference of relative performance between published and estimated
    let newResults = results.map((data) => {
      data["difference of relative performance"] =
        data["PRP: published relative performance"] -
        data["ERP: estimated relative performance from the original article"];
      return data;
    });

    const topTenDifference = newResults.sort(compareValues("difference of relative performance", "desc"));
    console.log(
      `Top 10 model name that have huge difference of relative performance between published and estimated >>`,
      topTenDifference.slice(0, 10).map((data, i) => ({ [i + 1]: data["Model Name"] }))
    );
  });
